from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _


class PhoneNumberValidator(RegexValidator):
    regex = r'^989[0-3,9]\d{8}$'
    message = _('Phone number must be a valid 12 digits number like 98xxxxxxxxxx')
    code = 'invalid_phone_number'


validate_phone_number = PhoneNumberValidator()
