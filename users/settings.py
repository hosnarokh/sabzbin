from django.conf import settings
from rest_framework.settings import APISettings

USER_SETTINGS = getattr(settings, 'USERS_CONFIG', None)

DEFAULTS = {
    'PROJECT_NAME': 'project',
    #
    # 'CAN_CHANGE_PHONE_NUMBER': False,
    #
    # 'SMS_VERIFICATION_HANDLER': 'users.utils.send_verification',

    # if you need to cipher phone number to avoid sms bombing or other security holes
    'ENCRYPTION_CLASS': 'users.utils.AESCipher',
    'ENCRYPTION_KEY': None,  # if exists all phone numbers must send in encryption form,

    # if you want to use google captcha
    'USE_CAPTCHA': False,
    'CAPTCHA_VALIDATION_URL': 'https://www.google.com/recaptcha/api/siteverify',
    'CAPTCHA_VALIDATION_TOKEN': '',

    # if you need to send SMSes or Emails use celery and queue(Async)
    'CELERY_VERIFICATION_SMS': False,
    'CELERY_VERIFICATION_EMAIL': False,

    # if it is not a Vas service(vas_service=0 these params must set)
    'SMS_VERIFICATION_TEMPLATE_PATH': '',
    'VERIFY_CODE_MIN': 10000,
    'VERIFY_CODE_MAX': 99999,
    'VERIFY_CODE_MAX_VALID_TIME': 15 * 60,
    'SMS_VERIFICATION_USERNAME': '',
    'SMS_VERIFICATION_PASSWORD': '',
    'SMS_VERIFICATION_SMS_NUMBER': '',
    'SMS_UNICODE': True,
}
ENCRYPTION_CLASS = 'users.utils.AESCipher'
ENCRYPTION_KEY = ''
users_settings = APISettings(USER_SETTINGS, DEFAULTS)
