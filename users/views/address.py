from django.contrib.auth import get_user_model
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from users.models import UserAddress
from users.serializers import UserAddressSerializer

User = get_user_model()


class UserAddressViewSet(viewsets.ModelViewSet):
    """
    GET/CREATE/UPDATE/DELETE users address info such as , name,longitude,latitude,users,address
    """
    serializer_class = UserAddressSerializer

    permission_classes = (IsAuthenticated,)
    lookup_field = 'uid'

    def get_queryset(self):
        return UserAddress.objects.filter(user=self.request.user).all()

    def get_serializer_context(self):
        context = super(UserAddressViewSet, self).get_serializer_context()
        if self.request:
            context.update({
                "user": self.request.user
            })
        return context
