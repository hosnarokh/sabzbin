from random import randint

from django.contrib.auth import get_user_model
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from users.models import UserScore
from users.serializers import RegisterSerializer, VerifySerializer
from users.serializers.score import ScoreSerializer

User = get_user_model()


class RegisterAPIView(generics.CreateAPIView):
    """
    post:
        API view that creates a new user and sends verification sms.

            body:
                phone_number: string/user phone number
                password: string/user password
    """
    serializer_class = RegisterSerializer
    queryset = User.objects.all()

    def perform_create(self, serializer):
        verify_code = str(randint(10000, 99999))
        serializer.save(verify_code=verify_code)
        # sent_message = send_verification(serializer.data['phone_number'], verify_code)
        # if not sent_message:
        #     raise ValidationError({'detail': _('Could not sent verification SMS')})


class ScoreListAPIView(generics.ListAPIView):
    serializer_class = ScoreSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return UserScore.objects.filter(user=self.request.user).all()


class VerifyAPIView(generics.CreateAPIView):
    """
    post:
        API view that check verify_code and verify user.

            body:
                phone_number: string/user phone number
                verify_code: string/user verify_code
    """
    serializer_class = VerifySerializer
    queryset = User.objects.all()
