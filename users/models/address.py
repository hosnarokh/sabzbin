from django.db import models
from django.utils.translation import ugettext_lazy as _

from utils.custom_models import BaseModel
from .user import User


class UserAddress(BaseModel):
    name = models.CharField(_('name'), max_length=50, null=True, blank=True)
    address_detail = models.TextField(_('address'))
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    user = models.ForeignKey(User, verbose_name=_('users'), related_name='user_addresses',
                             blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Address')
        verbose_name_plural = _('Addresses')
