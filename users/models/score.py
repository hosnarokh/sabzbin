from django.db import models
from django.utils.translation import ugettext_lazy as _

from utils.custom_models import BaseModel
from .user import User


class UserScore(BaseModel):
    PROFILE_SCORE = 1
    INVITE_FRIENDS_SCORE = 2
    SCORE_CHOICES = (
        (PROFILE_SCORE, _('profile')),
        (INVITE_FRIENDS_SCORE, _('invite friends'))
    )
    user = models.ForeignKey(User, verbose_name=_('users'), related_name='scores', on_delete=models.CASCADE)
    score_type = models.PositiveIntegerField(_('score type'), choices=SCORE_CHOICES, default=PROFILE_SCORE)
    points = models.PositiveIntegerField(_('points'), default=0)

    class Meta:
        verbose_name = _('User Score')
        verbose_name_plural = _('User Scores')


class UserScoreTransaction(BaseModel):
    PROFILE_SCORE = 1
    INVITE_FRIENDS_SCORE = 2
    SCORE_CHOICES = (
        (PROFILE_SCORE, _('profile')),
        (INVITE_FRIENDS_SCORE, _('invite friends'))
    )
    user = models.ForeignKey(User, verbose_name=_('users'), related_name='transactions', on_delete=models.CASCADE)
    transaction_type = models.PositiveIntegerField(_('transaction type'), choices=SCORE_CHOICES, default=PROFILE_SCORE)
    points = models.PositiveIntegerField(_('points'), default=0)

    class Meta:
        verbose_name = _('User Score Transaction')
        verbose_name_plural = _('User Score Transactions')


