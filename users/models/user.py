from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin)
from django.core import validators
from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


# from users.models import UserAddress


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, phone_number, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given phone_number and password.
        """
        now = timezone.now()
        user = self.model(phone_number=phone_number,
                          is_staff=is_staff,
                          is_superuser=is_superuser,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, phone_number=None, password=None, **extra_fields):
        return self._create_user(phone_number, password, False, False, **extra_fields)

    def create_superuser(self, phone_number, password, **extra_fields):
        return self._create_user(phone_number, password, True, True, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """
    first_name = models.CharField(_('first name'), max_length=30, blank=True, null=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True, null=True)
    email = models.EmailField(_('email address'), unique=True, blank=True, null=True)
    phone_number = models.CharField(_('mobile number'), max_length=120, unique=True,
                                    validators=[
                                        validators.RegexValidator(r'^989[0-3,9]\d{8}$',
                                                                  ('Enter a valid mobile number.'), 'invalid'),
                                    ],
                                    error_messages={
                                        'unique': _("A users with this mobile number already exists."),
                                    }
                                    )
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the users can logs into this admin site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this users should be treated as active. '
                                                'Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    is_verify = models.BooleanField(_('verify'), default=False, )
    verify_codes = models.CharField(verbose_name=_('verification codes'),
                                    validators=[validate_comma_separated_integer_list], max_length=30, blank=True)
    updated_time = models.DateTimeField(_('updated time'), auto_now=True)
    referral = models.ForeignKey('self', verbose_name=_('referral'), related_name='users', on_delete=models.SET_NULL,
                                 null=True, blank=True)
    birth_date = models.DateField(_('birthday date'), null=True, blank=True)

    REQUIRED_FIELDS = ['password']
    USERNAME_FIELD = 'phone_number'
    objects = UserManager()

    class Meta:
        verbose_name = _('users')
        verbose_name_plural = _('users')

    def get_verify_codes(self):
        if self.verify_codes:
            return self.verify_codes.split(',')
        else:
            return []

    def set_verify_code(self, verify_code):
        vlist = self.get_verify_codes()
        vlist.append(str(verify_code))
        self.verify_codes = ','.join(vlist[-5:])
