from django.contrib import admin
from khayyam import JalaliDate as jd

from users.models import UserAddress


@admin.register(UserAddress)
class UserAddressAdmin(admin.ModelAdmin):
    list_display = ['uid', 'name', 'address_detail', 'latitude', 'longitude', 'created_date', 'updated_date']
    fields = ['uid', 'name', 'user', 'latitude', 'longitude', 'created', 'updated', 'address_detail', ]
    search_fields = ['name']
    readonly_fields = ['uid', 'created', 'updated', 'created_date', 'updated_date']

    def created_date(self, obj):
        if obj.created:
            return jd(obj.created)
        return "-"

    def updated_date(self, obj):
        if obj.updated:
            return jd(obj.updated)
        return "-"
