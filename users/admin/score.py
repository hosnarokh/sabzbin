from django.contrib import admin
from khayyam import JalaliDate as jd

from users.models import UserScoreTransaction, UserScore


@admin.register(UserScore)
class UserScoreAdmin(admin.ModelAdmin):
    list_display = ['uid', 'score_type', 'points', 'created_date', 'updated_date']
    fields = ['uid', 'score_type', 'points', 'user',]
    list_filter = ['score_type']
    readonly_fields = ['uid', 'created', 'updated', 'created_date', 'updated_date']

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def created_date(self, obj):
        if obj.created:
            return jd(obj.created)
        return "-"

    def updated_date(self, obj):
        if obj.updated:
            return jd(obj.updated)
        return "-"


@admin.register(UserScoreTransaction)
class UserScoreTransactionAdmin(admin.ModelAdmin):
    list_display = ['uid', 'transaction_type', 'points', 'created_date', 'updated_date']
    fields = ['uid', 'transaction_type', 'points', 'user']
    list_filter = ['transaction_type']
    readonly_fields = ['uid', 'created', 'updated','created_date', 'updated_date']

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def created_date(self, obj):
        if obj.created:
            return jd(obj.created)
        return "-"

    def updated_date(self, obj):
        if obj.updated:
            return jd(obj.updated)
        return "-"
