from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token
from rest_framework_jwt.views import verify_jwt_token

from .views import (
    RegisterAPIView,
    UserAddressViewSet,
    ScoreListAPIView
)
from .views.user import VerifyAPIView

router = DefaultRouter()
router.register(r'address', UserAddressViewSet, basename='auth')
urlpatterns = router.urls

urlpatterns += [
    url(r'^jwt/login/', obtain_jwt_token),
    url(r'^jwt/token-refresh/', refresh_jwt_token),
    url(r'^jwt/verify/', verify_jwt_token),

    url(r'^register/$', RegisterAPIView.as_view()),
    url(r'^verify-phone-number/$', VerifyAPIView.as_view()),

    url(r'^scores/$', ScoreListAPIView.as_view()),

]
