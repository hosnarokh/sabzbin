from django.db.models.signals import post_save
from django.dispatch import receiver

from users.models import UserScoreTransaction, UserScore


@receiver(post_save, sender=UserScoreTransaction)
def create_notification_follow(sender, instance,created, **kwargs):
    if created:
        user_score, create = UserScore.objects.get_or_create(score_type=instance.transaction_type, user_id=instance.user_id)
        user_score.points += instance.points
        user_score.save()