import requests
from django.template.loader import render_to_string
from rest_framework.exceptions import ValidationError, AuthenticationFailed

from .settings import users_settings


def send_verification(phone_number, verify_code):
    """
    sending verification code to the phone number

    :param phone_number: the user phone number
    :param verify_code: the verification code

    :raise ValidationError: if sending a message failed
    """

    message = render_to_string(users_settings.SMS_VERIFICATION_TEMPLATE_PATH, {
        'verify_code': verify_code
    })

    query_params = {
        'username': users_settings.SMS_VERIFICATION_USERNAME,
        'password': users_settings.SMS_VERIFICATION_PASSWORD,
        'srcaddress': users_settings.SMS_VERIFICATION_SMS_NUMBER,
        'dstaddress': phone_number,
        'body': message,
        'unicode': users_settings.SMS_UNICODE,
    }

    try:
        response = requests.get(users_settings.SMS_VERIFICATION_URI,
                                params=query_params, timeout=60)

        response.raise_for_status()

        if response.ok and 'ERR' in response.text:
            raise AuthenticationFailed(detail='Server authentication failed')

    except Exception as e:
        raise ValidationError({'detail': 'Could not sent SMS verification'})
