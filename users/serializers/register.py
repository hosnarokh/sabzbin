from django.utils import timezone

from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.validators import UniqueValidator
from django.conf import settings
from users.validators import validate_phone_number

User = get_user_model()


class RegisterSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(max_length=120, )
    password = serializers.CharField(max_length=128, required=False)

    class Meta:
        model = User
        fields = ('phone_number', 'password')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for i, v in enumerate(self.fields['phone_number'].validators):
            if isinstance(v, UniqueValidator):
                del self.fields['phone_number'].validators[i]

    def create(self, validated_data):
        phone_number = validated_data.get('phone_number')
        password = validated_data.get('password')

        try:
            validate_phone_number(phone_number)
        except ValidationError as v_err:
            raise ValidationError(v_err.message)
        except Exception:
            raise ValidationError(
                {'message': _('phone number is invalid')}, code=400
            )

        if not phone_number:
            raise ValidationError(
                {'message': _('send phone number ')}, code=400
            )

        validated_data.update({'phone_number': phone_number})

        try:
            instance = User.objects.get(phone_number=validated_data['phone_number'])
        except User.DoesNotExist:
            instance = User.objects.create_user(
                phone_number=validated_data['phone_number'],
                verify_codes=validated_data['verify_code'],
                password=password
            )
        else:
            instance.set_verify_code(validated_data['verify_code'])
            instance.password = password
            instance.save(update_fields=['verify_codes'], )

        return instance


class VerifySerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(max_length=120, )
    verify_codes = serializers.CharField(max_length=30, )

    class Meta:
        model = User
        fields = ('phone_number', 'verify_codes')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for i, v in enumerate(self.fields['phone_number'].validators):
            if isinstance(v, UniqueValidator):
                del self.fields['phone_number'].validators[i]

    def create(self, validated_data):
        phone_number = validated_data['phone_number']
        verify_codes = validated_data['verify_codes']

        try:
            validate_phone_number(phone_number)
        except ValidationError as v_err:
            raise ValidationError(v_err.message)
        except Exception:
            raise ValidationError(
                {'message': _('phone number is invalid')}, code=400
            )
        validated_data.update({'phone_number': phone_number})

        try:
            instance = User.objects.get(phone_number=validated_data['phone_number'])
        except User.DoesNotExist:
            raise ValidationError(
                {'message': _('phone number is invalid')}, code=400
            )
        else:
            if instance.updated_time + timezone.timedelta(
                    minutes=int(settings.EXPIRE_TIME_VERIFY_CODE)) < timezone.now():
                raise ValidationError(
                    {'message': _('verify code timeout')}, code=400
                )
            if instance.verify_codes.split(',', )[-1] == verify_codes:
                instance.is_verify = True
                instance.save()
                return instance
            else:
                raise ValidationError(
                    {'message': _('verify code is invalid')}, code=400
                )