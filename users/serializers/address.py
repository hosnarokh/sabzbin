from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from users.models import UserAddress

User = get_user_model()


class UserAddressSerializer(serializers.ModelSerializer):
    """
    A serializer for users address that contains  'name','longitude','latitude','users','address'
    """

    class Meta:
        model = UserAddress
        fields = ('uid','name', 'address_detail', 'latitude', 'longitude','created','updated')
        read_only_fields = ('uid','user','created','updated')
        extra_kwargs = {
            'url': {'lookup_field': 'uid'}
        }

    def create(self, validated_data):
        user = self.context.get('user')
        longitude = validated_data.get('longitude')
        latitude = validated_data.get('latitude')
        has_user_address = UserAddress.objects.filter(user=user, longitude=longitude,
                                                      latitude=latitude).all()
        if has_user_address:
            raise serializers.ValidationError(_('A Address with this latitude and longitude already exists.'))

        instance = UserAddress.objects.create(user=user,
                                              **validated_data)
        return instance

    def get(self,request):
        print("laila")
        pass

