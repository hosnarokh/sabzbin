from django.contrib.auth import get_user_model
from rest_framework import serializers

from users.models import UserScore, UserScoreTransaction

User = get_user_model()


class UserTransactionsSerailizer(serializers.ModelSerializer):
    class Meta:
        model = UserScoreTransaction
        fields = ('uid', 'points', 'created', 'updated')
        read_only_fields = ('uid', 'created', 'updated')


class ScoreSerializer(serializers.ModelSerializer):
    score_type_text = serializers.SerializerMethodField()

    def get_score_type_text(self, score):
        return score.get_score_type_display()

    class Meta:
        model = UserScore
        fields = ('uid', 'score_type', 'score_type_text', 'points', 'created', 'updated',)
        read_only_fields = ('uid', 'created', 'updated')
        extra_kwargs = {
            'url': {'lookup_field': 'uid'}
        }
